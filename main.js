// written by: hb 6Feb17

// load our libraries
var express = require("express");
var path = require("path");

/*
console.log(">>> express: ", express);
console.log(">>> path: ", path);
*/

// create an instance of express application
// an "instance" is a running version (like a memory image) of a template
var app = express();
var app2 = express();

// define routes 
// route 1
app.use(express.static(__dirname + "/public")); // __dirname is dir Node starts
fullPath = __dirname + "/public";
console.log(">> ", __dirname);
console.log("\public >> ", fullPath);

// route 2 ~ send custom error msg
app.use(function(req, res) {
    res.status(404);
    res.type("text/html"); // Representation of resource
    res.send("<h1>File not file</h1><p>Current time: " + new Date() +"</p");
});

// setting port as a property of the application
app.set("port", 3000); // key, value pair
app2.set("port", 2555);

// start the main server on the specific port
app.listen(app.get("port"), function() {
    console.log("App is listening on port " + app.get("port"));
    console.log("1st Connected!...");
});

app2.listen(app2.get("port"), function() {
    console.log("App2 is listening on port " + app2.get("port"));
    console.log("2nd Connected!...");
});

// localhost:3000    where localhost is 127.0.0.1
// console.info("Port: ", app.get("port"));

// var x = app.new








// End
